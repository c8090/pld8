#include <iostream>
#include <cstdlib>

// Pseudocode PLD Chapter 8 #1 pg. 361
// Name: Jonathan McGaha
// Date: Friday, April 1, 2022
// Start
//     Declarations
//         num SIZE = 10
//         num NUMBERS[SIZE]
//         num i
//         num j
//         num temp
//     for i = 0 to SIZE - 1
//         output "Please enter a number: "
//         input NUMBERS[i]
//     endfor
//     for i = 0 to SIZE - 2
//         for j = 0 to SIZE - 2
//             if (NUMBERS[j] < NUMBERS[j+1])
//               temp = NUMBERS[j]
//               NUMBERS[j] = NUMBERS[j+1]
//               NUMBERS[j+1] = temp
//             endif
//         endfor
//     endfor
//     output "Sorted List"
//     output "==========="
//     for i = 0 to SIZE - 1
//         output "Number ", i + 1, ": ", NUMBERS[i]
//     endfor
// Stop

using namespace std;

int main() {
    int SIZE = 10;
    int NUMBERS[SIZE];
    int i;
    int j;
    int temp;
    for (i = 0; i < SIZE - 1; i++) {
        cout << "Please enter a number: ";
        cin >> NUMBERS[i];
    }
    for (i = 0; i < SIZE - 2; i ++) {
        for (j = 0; j < SIZE - 2; j++) {
            if (NUMBERS[j] < NUMBERS[j+1]) {
                temp = NUMBERS[j];
                NUMBERS[j] = NUMBERS[j+1];
                NUMBERS[j+1] = temp;
            }
        }
    }
    cout << "Sorted List" << endl;
    cout << "============" << endl;
    for (i = 0; i < SIZE - 1; i++) {
        cout << "Number " << i + 1 << ": " << NUMBERS[i] << endl;
    }
    system("PAUSE");
    return 0;
}
